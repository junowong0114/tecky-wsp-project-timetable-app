import express from 'express';
import { client } from './app';
import { isLoggedIn } from './guard';
import { generateCourseJSON, Course } from './lib';

export const timetableRoutes = express.Router();

timetableRoutes.get('/timetable', isLoggedIn, async (req, res) => {
    const page = 'timetable';
    res.render('protected/timetable', { page: page, user: req.session['user'] });
});

timetableRoutes.get('/timetableIndex', isLoggedIn, async (req, res) => {
    let result = await client.query('SELECT timetable_id from timetables where student_id = $1', [req.session['student_id']]);
    let timetableIndex = result.rows.length;
    res.json(timetableIndex);
});


timetableRoutes.post('/timetable', async (req, res) => {
    const sessionIDs = req.body.sessionIDs;
    const result = await client.query('INSERT INTO timetables (student_id) values ($1) returning timetable_id', [req.session['student_id']])
    const timetableID = result.rows[0].timetable_id;
    for(let sessionID of sessionIDs){
        await client.query('INSERT INTO timetable_session (timetable_id, session_id) values ($1,$2)', [timetableID, sessionID])
    }
    res.json(timetableID);
});

timetableRoutes.put('/timetable', async (req, res) => {
    const sessionIDs = req.body.sessionIDs;
    const timetableIndex = req.body.timetableIndex;
    let result = await client.query('SELECT timetable_id from timetables where student_id = $1', [req.session['student_id']]);
    let timetableIDsJson = result.rows;
    let timetableID = timetableIDsJson[timetableIndex].timetable_id;
    await client.query('DELETE FROM timetable_session WHERE timetable_id = $1', [timetableID])
    for(let sessionID of sessionIDs){
        await client.query('INSERT INTO timetable_session (timetable_id, session_id) values ($1,$2)', [timetableID, sessionID])
    }
    res.json({success:true});
});

timetableRoutes.delete('/timetable', async (req, res) => {
    const timetableIndex = req.body.timetableIndex;  
    let result = await client.query('SELECT timetable_id from timetables where student_id = $1', [req.session['student_id']]);
    let timetableIDsJson = result.rows;
    let timetableID = timetableIDsJson[timetableIndex].timetable_id;
    await client.query('DELETE FROM timetable_session WHERE timetable_id = $1', [timetableID])
    await client.query('DELETE FROM timetables WHERE timetable_id = $1', [timetableID])
    res.json({success:true});
});

timetableRoutes.get('/timetableIDs', async (req, res) => {
    let result = await client.query('SELECT timetable_id from timetables where student_id = $1', [req.session['student_id']]);
    let timetableIDsJson = result.rows;
    let timetableIDs: number[] = [];
    for (let timetableIDJson of timetableIDsJson){
        timetableIDs.push(timetableIDJson.timetable_id)
    }
    res.json(timetableIDs);
});

timetableRoutes.get('/sessionIDs', async (req, res) => {
    const course = req.query.course as string | undefined;
    const timetable_id  = req.query.timetableID as string | undefined;
    let subjectID;
    let courseCode;
    if(course){
        subjectID = course.substring(0,4).toLowerCase();
        courseCode = course.substring(4);
    }    
    let sessionIDs = [];
    if(timetable_id){
        let result = await client.query('SELECT session_id FROM timetable_session JOIN timetables ON timetables.timetable_id = timetable_session.timetable_id WHERE student_id = $1 AND timetable_session.timetable_id = $2', [req.session['student_id'], timetable_id]);
        let sessionIDsJson = result.rows;        
        for (let sessionIDJson of sessionIDsJson) {
            sessionIDs.push(sessionIDJson.session_id);
        }        
    }
    else{
        let lectureIDs = await client.query('SELECT lecture_id FROM lectures JOIN courses ON lectures.course_id = courses.course_id WHERE subject_id = $1 AND course_code = $2', [subjectID, courseCode]);
        for(let lectureID of lectureIDs.rows){
            let results = await client.query('SELECT session_id FROM lectures JOIN sessions ON lectures.lecture_id = sessions.lecture_id WHERE lectures.lecture_id = $1', [lectureID.lecture_id]);
            let sessionID = [];
            for(let result of results.rows){
                sessionID.push(result.session_id);
            }
            sessionIDs.push(sessionID);
        }
        let tutorialIDs = await client.query('SELECT tutorial_id FROM tutorials JOIN courses ON tutorials.course_id = courses.course_id WHERE subject_id = $1 AND course_code = $2', [subjectID, courseCode]);
        for(let tutorialID of tutorialIDs.rows){
            let results = await client.query('SELECT session_id FROM tutorials JOIN sessions ON tutorials.tutorial_id = sessions.tutorial_id WHERE tutorials.tutorial_id = $1', [tutorialID.tutorial_id]);
            let sessionID = [];
            for(let result of results.rows){
                sessionID.push(result.session_id);
            }
            sessionIDs.push(sessionID);
        }
        let labIDs = await client.query('SELECT lab_id FROM labs JOIN courses ON labs.course_id = courses.course_id WHERE subject_id = $1 AND course_code = $2', [subjectID, courseCode]);
        for(let labID of labIDs.rows){
            let results = await client.query('SELECT session_id FROM labs JOIN sessions ON labs.lab_id = sessions.lab_id WHERE labs.lab_id = $1', [labID.lab_id]);
            let sessionID = [];
            for(let result of results.rows){
                sessionID.push(result.session_id);
            }
            sessionIDs.push(sessionID);
        }
        let researchIDs = await client.query('SELECT research_id FROM researches JOIN courses ON researches.course_id = courses.course_id WHERE subject_id = $1 AND course_code = $2', [subjectID, courseCode]);
        for(let researchID of researchIDs.rows){
            let results = await client.query('SELECT session_id FROM researches JOIN sessions ON researches.research_id = sessions.research_id WHERE researches.research_id = $1', [researchID.research_id]);
            let sessionID = [];
            for(let result of results.rows){
                sessionID.push(result.session_id);
            }
            sessionIDs.push(sessionID);
        }
    }
    res.json(sessionIDs);
});

timetableRoutes.get('/session', async (req, res) => {
    const session_id  = req.query.sessionID;
    let result = await client.query('SELECT * FROM sessions WHERE session_id = $1', [session_id]);
    let session = result.rows[0];
    res.json(session);
});

timetableRoutes.get('/activity', async (req, res) => {
    const session_id  = req.query.sessionID;
    let result = await client.query('SELECT lecture_id, tutorial_id, lab_id, research_id FROM sessions WHERE session_id = $1', [session_id]);
    let session = result.rows[0];
    if(session.lecture_id != null){
        result = await client.query('SELECT * FROM lectures WHERE lecture_id = $1', [session.lecture_id])
    }    
    else if(session.tutorial_id != null){
        result = await client.query('SELECT * FROM tutorials WHERE tutorial_id = $1', [session.tutorial_id])
    }
    else if(session.lab_id != null){
        result = await client.query('SELECT * FROM labs WHERE lab_id = $1', [session.lab_id])
    }
    else{
        result = await client.query('SELECT * FROM researches WHERE research_id = $1', [session.research_id])
    }
    let activity = result.rows[0];
    res.json(activity);
});

timetableRoutes.get('/course', async (req, res) => {
    const course_id  = req.query.courseID;
    let result = await client.query('SELECT * FROM courses WHERE course_id = $1', [course_id]);
    let course = result.rows[0];
    res.json(course);
});

timetableRoutes.get('/subject', async (req, res) => {
    const subject_id  = req.query.subjectID;
    let result = await client.query('SELECT * FROM subjects WHERE subject_id = $1', [subject_id]);
    let subject = result.rows[0];
    res.json(subject);
});

timetableRoutes.get('/department', async (req, res) => {
    const department_id  = req.query.departmentID;
    let result = await client.query('SELECT * FROM departments WHERE department_id = $1', [department_id]);
    let department = result.rows[0];
    res.json(department);
});

timetableRoutes.get('/starredCourseTitles', async (req, res) => {
    let result = await client.query(`SELECT DISTINCT ON (subject_id) subject_id, course_code, common_core_areas.common_core_area_id as common_core_area 
        FROM students 
        JOIN starred_courses ON starred_courses.student_id = $1 
        JOIN courses ON starred_courses.course_id = courses.course_id 
        LEFT OUTER JOIN common_core_courses ON courses.course_id = common_core_courses.course_id
        LEFT OUTER JOIN common_core_areas ON common_core_courses.common_core_area_id = common_core_areas.common_core_area_id`
        , [req.session['student_id']]);

    let starredCourseTitles = result.rows;
    res.json(starredCourseTitles);
});

timetableRoutes.get('/starredCourses', async (req, res) => {
    let areaFilters = req.query.areaFilter as string | string[] | undefined;

    if (typeof areaFilters === 'string') areaFilters = [areaFilters] as string[];

    /* construct the condition string for database query */
    const conditions: string[] = [];
    let count = 2;
    const values: string[] = [req.session['student_id']];

    let areaConditionString = '';
    if (areaFilters) {
        const areaConditions: string[] = [];
        for (let filter of areaFilters) {
            areaConditions.push(`common_core_areas.common_core_area_id = LOWER($${count++})`);
            values.push(filter);
        }
        areaConditionString = '(' + areaConditions.join(' OR ') + ')';
        conditions.push(areaConditionString);
    }

    const conditionString = conditions.join(' AND ');

    let result = await client.query(`SELECT DISTINCT ON (courses.course_id) courses.course_id as id, subjects.subject_id as subject, 
        course_code as code, course_name as title, credit, common_core_areas.common_core_area_id as common_core_area
        FROM courses
        JOIN subjects ON courses.subject_id = subjects.subject_id
        RIGHT OUTER JOIN starred_courses ON starred_courses.course_id = courses.course_id
        LEFT OUTER JOIN common_core_courses ON courses.course_id = common_core_courses.course_id
        LEFT OUTER JOIN common_core_areas ON common_core_courses.common_core_area_id = common_core_areas.common_core_area_id
        WHERE starred_courses.student_id = $1 ${conditionString === '' ? '' : 'AND'} ${conditionString}`, values);

    const courses = result.rows;
    const courseObjs: Course[] = await generateCourseJSON(courses);

    if (courseObjs.length) {
        return res.json({
            success: true,
            found: courseObjs.length,
            courses: courseObjs
        });
    }

    return res.json({
        success: false,
        found: 0,
        courses: []
    })
});