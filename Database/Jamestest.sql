SELECT * FROM students;
SELECT * FROM departments;
SELECT * FROM subjects;
SELECT * FROM courses;
SELECT * FROM instructors;
SELECT * FROM lectures;
SELECT * FROM tutorials;
SELECT * FROM labs;
SELECT * FROM researches;
SELECT * FROM common_core_areas;
SELECT * FROM common_core_courses;
SELECT * FROM sessions;
SELECT * FROM timetable_session;
SELECT * FROM timetables;
SELECT * FROM starred_courses;
SELECT * FROM must_take_courses;
SELECT * FROM course_preferences;
SELECT * FROM config_timeslots;
SELECT * FROM must_take_common_core_areas;

--Timetable test case--
INSERT INTO starred_courses (student_id, course_id) values (1,1);
INSERT INTO starred_courses (student_id, course_id) values (1,158);
INSERT INTO timetables (student_id) values (1);
INSERT INTO timetable_session (timetable_id, session_id) values (1,1);
INSERT INTO timetable_session (timetable_id, session_id) values (1,2);

--example--
SELECT courses.subject_id, course_code, course_name, credit FROM courses 
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE department_name = 'Science' AND courses.subject_id = 'CHEM';
SELECT subject_id FROM subjects JOIN departments ON subjects.department_id = departments.department_id WHERE department_name LIKE 'Science';

--test sessionIDs--
SELECT session_id FROM timetable_session 
JOIN timetables ON timetables.timetable_id = timetable_session.timetable_id
WHERE student_id = 1 AND timetable_session.timetable_id = 1


