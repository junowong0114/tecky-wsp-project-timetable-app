SELECT * FROM students;
SELECT * FROM departments;
SELECT * FROM subjects;
SELECT * FROM courses;
SELECT * FROM instructors;
SELECT * FROM lectures;
SELECT * FROM tutorials;
SELECT * FROM labs;
SELECT * FROM researchs;
SELECT * FROM common_core_areas;
SELECT * FROM common_core_courses;
SELECT * FROM sessions;
SELECT * FROM timetable_session;
SELECT * FROM timetables;
SELECT * FROM starred_courses;
SELECT * FROM must_take_courses;
SELECT * FROM course_preferences;
SELECT * FROM config_timeslots;
SELECT * FROM must_take_common_core_areas;

/* select all schools */
SELECT department_name as name from departments;

/* select subjects that matches school filter */
SELECT subject_id FROM subjects
JOIN departments ON subjects.department_id = departments.department_id
WHERE department_name = LOWER('science') OR department_name = LOWER('engineering');

/* select all course info that matches filters */
SELECT course_id, courses.subject_id, course_code, course_name, credit FROM courses 
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE department_name = 'Engineering' AND courses.subject_id = 'COMP';

/* select all lectures from COMP 1021 */
SELECT * FROM lectures
JOIN courses ON lectures.course_id = courses.course_id
JOIN sessions ON sessions.lecture_id = lectures.lecture_id
JOIN instructors ON instructors.instructor_id = lectures.instructor_id
WHERE courses.course_id = 148;

SELECT lecture_name, weekday, start_time, end_time, venue, first_name, last_name FROM lectures
JOIN courses ON lectures.course_id = courses.course_id
JOIN sessions ON sessions.lecture_id = lectures.lecture_id
JOIN instructors ON instructors.instructor_id = lectures.instructor_id
WHERE courses.course_id = 148;

/* select all common cores */
SELECT courses.course_id, subject_id, course_code, course_name, credit, common_core_areas.common_core_area_id FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id;

/* select common cores with filter */
SELECT courses.course_id, subjects.subject_id, course_code, course_name, credit, common_core_areas.common_core_area_name FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE (subjects.subject_id = LOWER('comp') OR subjects.subject_id = LOWER('mech')) AND departments.department_name = LOWER('engineering');

SELECT courses.course_id, subjects.subject_id, course_code, course_name, credit, common_core_areas.common_core_area_id FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE common_core_areas.common_core_area_id = LOWER('s&t');

/* total number of common cores */
SELECT COUNT(*) FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id;

/* total number of courses */
SELECT COUNT(*) FROM courses
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id;

/* select all lectures of given course_id */
SELECT lectures.lecture_id as id, lectures.lecture_name as name, sessions.weekday as date,
sessions.start_time as startTime, sessions.end_time as endTime, sessions.venue as venue,
instructors.first_name as firstName, instructors.last_name as lastName
FROM lectures
JOIN courses ON lectures.course_id = courses.course_id
JOIN sessions ON lectures.lecture_id = sessions.lecture_id
JOIN instructors ON instructors.instructor_id = lectures.instructor_id
WHERE courses.course_id = 1;

/* select all tutorials of given course_id */
SELECT tutorials.tutorial_id as id, tutorials.tutorial_name as name, sessions.weekday as date,
sessions.start_time as startTime, sessions.end_time as endTime, sessions.venue as venue,
instructors.first_name as firstName, instructors.last_name as lastName
FROM tutorials
JOIN courses ON tutorials.course_id = courses.course_id
JOIN sessions ON tutorials.tutorial_id = sessions.tutorial_id
JOIN instructors ON instructors.instructor_id = tutorials.instructor_id
WHERE courses.course_id = 206;

/* select all researches of given course_id */
SELECT researchs.research_id as id, researchs.research_name as name, sessions.weekday as date,
sessions.start_time as startTime, sessions.end_time as endTime, sessions.venue as venue,
instructors.first_name as firstName, instructors.last_name as lastName
FROM researchs
JOIN courses ON researchs.course_id = courses.course_id
JOIN sessions ON researchs.research_id = sessions.research_id
JOIN instructors ON instructors.instructor_id = researchs.instructor_id
WHERE courses.course_id = 155;

/* query with/without common core filter */
SELECT courses.course_id as id, subjects.subject_id as subject, course_code as code, 
course_name as title, credit, common_core_areas.common_core_area_name as commonCoreArea FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE subjects.subject_id = LOWER('acct');

SELECT courses.course_id as id, subjects.subject_id as subject, course_code as code, 
course_name as title, credit FROM courses
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE subjects.subject_id = LOWER('acct');

SELECT DISTINCT ON (courses.course_id) courses.course_id as id, subjects.subject_id as subject, 
            course_code as code, course_name as title, credit, common_core_areas.common_core_area_name as common_core_area 
            FROM courses
            JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
            JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
            JOIN subjects ON courses.subject_id = subjects.subject_id
            JOIN departments ON subjects.department_id = departments.department_id



/* Get all common cores in the specified area(s) */
SELECT DISTINCT ON (courses.course_id) courses.course_id as id, subjects.subject_id as subject, course_code as code, 
course_name as title, credit, common_core_areas.common_core_area_name as commonCoreArea FROM courses
JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
JOIN subjects ON courses.subject_id = subjects.subject_id
JOIN departments ON subjects.department_id = departments.department_id
WHERE common_core_areas.common_core_area_id = 's&t' OR common_core_areas.common_core_area_id = 'hlth';



INSERT INTO starred_courses (student_id, course_id) VALUES (1, 345);

SELECT subject_id, course_code, common_core_areas.common_core_area_id as common_core_area
        FROM students 
        JOIN starred_courses ON starred_courses.student_id = 1 
        JOIN courses ON starred_courses.course_id = courses.course_id 
        JOIN common_core_courses ON courses.course_id = common_core_courses.course_id
        JOIN common_core_areas ON common_core_courses.common_core_area_id = common_core_areas.common_core_area_id;


/* get favrourite courses */
SELECT courses.course_id as id, subjects.subject_id as subject, 
course_code as code, course_name as title, credit, common_core_areas.common_core_area_id as common_core_area
FROM courses
JOIN subjects ON courses.subject_id = subjects.subject_id
RIGHT OUTER JOIN starred_courses ON starred_courses.course_id = courses.course_id
LEFT OUTER JOIN common_core_courses ON courses.course_id = common_core_courses.course_id
LEFT OUTER JOIN common_core_areas ON common_core_courses.common_core_area_id = common_core_areas.common_core_area_id
WHERE starred_courses.student_id = 1;

SELECT courses.course_id
FROM courses
JOIN subjects ON subjects.subject_id = courses.subject_id
WHERE (subjects.subject_id = LOWER('CENG') AND courses.course_code = LOWER('4670')) 
OR (subjects.subject_id = LOWER('ENVR') AND courses.course_code = LOWER('1080'));

REMOVE FROM must_take_courses WHERE student_id = 1;

SELECT courses.course_id 
FROM courses 
JOIN subjects ON subjects.subject_id = courses.subject_id
WHERE subjects.subject_id = LOWER('ACCT') AND courses.course_code = LOWER('1610')

SELECT lectures.lecture_id as id, lectures.lecture_name as name, sessions.weekday as date,
            sessions.start_time as start_time, sessions.end_time as end_time, sessions.venue as venue,
            instructors.first_name as first_name, instructors.last_name as last_name, sessions.session_id as session_id
            FROM lectures
            JOIN courses ON lectures.course_id = courses.course_id
            JOIN sessions ON lectures.lecture_id = sessions.lecture_id
            JOIN instructors ON instructors.instructor_id = lectures.instructor_id