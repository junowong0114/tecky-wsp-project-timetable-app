import { Client } from 'pg';
import dotenv from 'dotenv';
import departments from './departments.json';
import subjects from './subjects.json';
import courses from './courses.json';
import activities from './activities.json';
import commonCoreAreas from './common_core_areas.json';
import commonCoreCourses from './common_core_courses.json';
import { Activity, ActivityTypeId, Instructor, ActivityType, ActivityTypeTableName, ActivityTypeName, ActivityWithTypeDetails, ActivityWithType } from './models';
import * as fs from "fs";

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

async function main() {
    await client.connect();

    const tableCreateSql: string = fs.readFileSync('./tableCreate.sql').toString();
    await client.query(tableCreateSql)

    const studentCreateSql: string = fs.readFileSync('./studentCreate.sql').toString();
    await client.query(studentCreateSql)

    for (let department of departments) {
        await client.query('INSERT INTO departments (department_name) values ($1) returning department_id', [department.name]);
    }
    for (let subject of subjects) {
        await client.query('INSERT INTO subjects (subject_id, subject_name, department_id) values ($1,$2,$3)', [subject.subject_id, subject.subject_name, subject.department_id]);
    }
    for (let course of courses) {
        await client.query('INSERT INTO courses (course_code, course_name, course_name_lower, credit, subject_id) values ($1,$2,lower($3),$4,$5) returning course_id', [course.code, course.title, course.title, course.credit, course.subject]);
    }
    for (let commonCoreArea of commonCoreAreas) {
        await client.query('INSERT INTO common_core_areas (common_core_area_id, common_core_area_name) values ($1, $2) ', [commonCoreArea.id, commonCoreArea.name])
    }
    for (let commonCoreCourse of commonCoreCourses) {
        await client.query('INSERT INTO common_core_courses (course_id, common_core_area_id) values ($1, $2) ', [await getCourseId(commonCoreCourse.subject, commonCoreCourse.code), await getCommonCoreAreaId(commonCoreCourse.area)])
    }
    let instructors: Instructor[] = await getInstructors();
    for (let instructor of instructors) {
        await client.query('INSERT INTO instructors (first_name, last_name) values ($1,$2) returning instructor_id', [instructor.first_name, instructor.last_name]);
    }
    let lectures: ActivityWithType[] = await getActivitiesType(0);
    for (let lecture of lectures) {
        await client.query('INSERT INTO lectures (lecture_name, course_id, instructor_id) values ($1,$2,$3) returning lecture_id', [lecture.name, lecture.course_id, lecture.instructor_id]);
    }
    let tutorials: ActivityWithType[] = await getActivitiesType(1);
    for (let tutorial of tutorials) {
        await client.query('INSERT INTO tutorials (tutorial_name, course_id, instructor_id) values ($1,$2,$3) returning tutorial_id', [tutorial.name, tutorial.course_id, tutorial.instructor_id]);
    }
    let labs: ActivityWithType[] = await getActivitiesType(2);
    for (let lab of labs) {
        await client.query('INSERT INTO labs (lab_name, course_id, instructor_id) values ($1,$2,$3) returning lab_id', [lab.name, lab.course_id, lab.instructor_id]);
    }
    let researches: ActivityWithType[] = await getActivitiesType(3);
    for (let research of researches) {
        await client.query('INSERT INTO researches (research_name, course_id, instructor_id) values ($1,$2,$3) returning research_id', [research.name, research.course_id, research.instructor_id]);
    }
    for (let activity of activities) {
        let activityType = activity.type;
        let venue = null;
        if (activity.venue != "TBA") {
            venue = activity.venue;
        }
        await client.query(`INSERT INTO sessions (venue, weekday, start_time, end_time, ${ActivityTypeId[activityType]}) values ($1, $2, $3, $4, $5) returning session_id`, [venue, activity.weekday, convertToTime(activity.start_time), convertToTime(activity.end_time), await getActivityWithTypeId(activity, activityType)])
    }

    await client.end();
}
main();

async function getInstructors() {
    let instructors: Instructor[] = [];
    for (let activity of activities) {
        let name: string[] = activity.instructor.split(", ");
        let result = false;
        if (name[0] == "TBA") {
            continue;
        }
        for (let instructor of instructors) {
            if (instructor.first_name == name[0] && instructor.last_name == name[1]) {
                result = true;
                break;
            }
        }
        if (!result) {
            instructors.push({
                first_name: name[0],
                last_name: name[1]
            })
        }
    }
    return instructors;
}

async function getActivitiesType(type: ActivityType) {
    let activitiesWithTypeDetails: ActivityWithTypeDetails[] = [];
    let activitiesWithType: ActivityWithType[] = [];
    for (let activity of activities) {
        if (activity.type == type) {
            let result = false;
            for (let activityWithTypeDetails of activitiesWithTypeDetails) {
                if (activityWithTypeDetails.name == activity.name && activityWithTypeDetails.subject == activity.course_subject && activityWithTypeDetails.course_code == activity.course_code) {
                    result = true;
                    break;
                }
            }
            if (!result) {
                activitiesWithTypeDetails.push({
                    name: activity.name,
                    subject: activity.course_subject,
                    course_code: activity.course_code,
                    instructor_name: activity.instructor
                })
            }
        }
    }
    for (let activityWithTypeDetails of activitiesWithTypeDetails) {
        activitiesWithType.push({
            name: activityWithTypeDetails.name,
            course_id: await getCourseId(activityWithTypeDetails.subject, activityWithTypeDetails.course_code),
            instructor_id: await getInstructorId(activityWithTypeDetails.instructor_name)
        });
    }
    return activitiesWithType;
}

async function getInstructorId(instructor_name: string) {
    let instructor;
    let instructor_id;
    let name: string[] = instructor_name.split(", ");
    if (name[0] == "TBA") {
        return;
    }
    else {
        instructor = await client.query('SELECT instructor_id FROM instructors WHERE first_name = $1 AND last_name = $2', [name[0], name[1]]);
        instructor_id = instructor.rows[0].instructor_id;
    }
    return instructor_id;
}

async function getCourseId(subject: string, course_code: string) {
    let result;
    let course_id;
    result = await client.query('SELECT course_id FROM courses WHERE subject_id = $1 AND course_code = $2', [subject, course_code]);
    course_id = result.rows[0].course_id;
    return course_id;
}

async function getCommonCoreAreaId(area: string) {
    let common_core_area_id: string;
    let result = await client.query('SELECT common_core_area_id FROM common_core_areas WHERE common_core_area_name = $1', [area]);
    common_core_area_id = result.rows[0].common_core_area_id;
    return common_core_area_id;
}

async function getActivityWithTypeId(activity: Activity, activityType: ActivityType) {
    let activityWithType_id;
    let activityWithTypeTableName: string = ActivityTypeTableName[activityType];
    let activityWithType_name: string = ActivityTypeName[activityType];
    let result = await client.query(`SELECT ${ActivityTypeId[activityType]} FROM ${activityWithTypeTableName} WHERE course_id = $1 AND ${activityWithType_name} = $2`, [await getCourseId(activity.course_subject, activity.course_code), activity.name])
    activityWithType_id = Object.values(result.rows[0])[0];
    return activityWithType_id;
}

function convertToTime(timeString: string) {
    if (timeString == "TBA") {
        return;
    }
    return timeString + ":00";
}