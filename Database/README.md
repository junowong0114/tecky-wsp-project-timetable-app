# Procedure of Database initialization

1. CREATE DATABASE timetable
2. Connect DATABASE timetable with TECKY-WSP-PROJECT-TIMETABLE-APP
3. CREATE .env file
    .env Template:
        DB_NAME=timetable
        DB_USERNAME=postgres
        DB_PASSWORD=postgres
4. Run databaseInit.ts (npm start in database file)

Remarks: You can modified studentCreate.sql for student user init

You can reset the database by running tableDrop.sql