import { forEach } from './modules/lib.mjs';
import { collapseAllMenus, toggleTick } from './modules/courseMultiSelector.mjs';
import { optionBtnClicked, badgeCloseBtnClicked, commonCoreToggleClicked } from './modules/courseFilter.mjs';
import { activateCommonCoreBtn, deactivateCommonCoreBtn } from './modules/courseFilter.mjs';
import { unboundPopulateSchools, unboundPopulateSubjects, createBadge, deleteBadgeByContent } from './modules/courseDynamic.mjs';
import { populateCourses } from './modules/courseList.mjs';
import { getParentElementByClass } from './modules/courseFilter.mjs';

const selectedFilters = {
    schools: [],
    subjects: [],
    areas: [],
    commonCore: false
}

const populateSchools = unboundPopulateSchools.bind(selectedFilters);
const populateSubjects = unboundPopulateSubjects.bind(selectedFilters);

window.addEventListener('load', async () => {
    const multiSelectorBtns = document.querySelectorAll('.multi-selector');
    forEach(multiSelectorBtns, (index, multiSelectorBtn) => {
        multiSelectorBtn.classList.remove('multi-selector-hidden');
    });

    await populateSchools();
    await populateSubjects();
    await populateCourses();
});

document.addEventListener('filterChanged', async (event) => {
    let filters = selectedFilters[event.detail.type];
    let content = event.detail.content;
    const type = event.detail.type;
    const action = event.detail.action;
    const btn = event.detail.btn;

    if (type === 'schools' || type === 'subjects') {
        if (action === 'add') {
            filters.push(content);
            createBadge(content, type, content);
        } else {
            filters = filters.filter(filter => filter !== content);
            deleteBadgeByContent(content);
        }
    }

    if (type === 'areas') {
        if (action === 'add') {
            if (!selectedFilters.commonCore) {
                selectedFilters.commonCore = true;
                activateCommonCoreBtn();
                createBadge('Common cores', 'commonCores');
            }
            filters.push(content === 'sat' ? 's&t' : content);
            createBadge(btn.innerText, type, content);
        } else {
            filters = filters.filter(filter => filter !== (content === 'sat' ? 's&t' : content));

            if (!filters.length) {
                selectedFilters.commonCore = false;
                deactivateCommonCoreBtn();
                deleteBadgeByContent('Common cores');
            }

            document.querySelector(`.filter-badge[content=${content}]`).remove();
        }
    }

    if (type === 'commonCores') {
        if (action === 'add') {
            selectedFilters.commonCore = true;
            createBadge('Common cores', type, 'Common cores');
            activateCommonCoreBtn();
        } else {
            selectedFilters.commonCore = false;
            selectedFilters.areas = [];
            // TODO deleteAreaBadge();
            deleteBadgeByContent('Common cores');
            deactivateCommonCoreBtn();
        }
    }

    console.log(selectedFilters);

    /* 'draw' or 'erase' tick */
    if (type !== 'commonCores') toggleTick(btn);

    /* update global filter list */
    selectedFilters[type] = filters;

    /* refresh subjects if school filter list is updated */
    if (type === 'schools') {
        await populateSubjects(selectedFilters.schools, selectedFilters.commonCore);
    }

    await populateCourses(selectedFilters.schools, selectedFilters.subjects, selectedFilters.commonCore, selectedFilters.areas);
});

/* collapse all filter select menu if clicked div is not multi-selector */
window.addEventListener('click', collapseAllMenus);

/* multi-selector option is clicked */
const optionContainers = document.querySelectorAll('.options-container');
forEach(optionContainers, (index, optionContainer) => {
    optionContainer.addEventListener('click', optionBtnClicked.bind(selectedFilters));
});

/* close btn on badge clicked */
const badgeContainer = document.querySelector('#filter-badges-container');
badgeContainer.addEventListener('click', badgeCloseBtnClicked);

/* common core filter button clicked */
document.querySelector('#common-core-filter-toggle').addEventListener('click', commonCoreToggleClicked);

/* save favourite courses */
document.querySelector('#course-list-hero').addEventListener('click', async (event) => {
    if (!(event.target.tagName === "path")) return;

    const starPath = event.target;
    if (starPath.classList.contains('filled')) {
        starPath.classList.remove('filled');

        const container = getParentElementByClass(starPath, 'position-relative');
        const fullCourseCode = container.getAttribute('course');
        const subject = (fullCourseCode.split(' '))[0];
        const code = (fullCourseCode.split(' '))[1];
        const reqBody = {
            subject: subject,
            code: code
        }

        const res = await fetch('/starredCourse', {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(reqBody)
        });

        const result = await res.json();

        if (result.success) {
            console.log(`Successfully deleted ${subject}-${code} from favourite courses.`);
        }
    }
    else {
        starPath.classList.add('filled');

        const container = getParentElementByClass(starPath, 'position-relative');
        const fullCourseCode = container.getAttribute('course');
        const subject = (fullCourseCode.split(' '))[0];
        const code = (fullCourseCode.split(' '))[1];
        const reqBody = {
            subject: subject,
            code: code
        }

        const res = await fetch('/starredCourse', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(reqBody)
        });

        const result = await res.json();

        if (result.success) {
            console.log(`Successfully added ${subject}-${code} to favourite courses.`);
        }
    }
});