import { forEach } from './lib.mjs';

export const collapseAllMenus = (event) => {
    const elem = event.target;

    if (!checkParentElementRecursive(elem, 'multi-selector')) {
        try {
            const multiSelectors = document.querySelectorAll('[elemType="multi-selector"]');
            forEach(multiSelectors, (index, multiSelector) => {
                const myCollapse = multiSelector.querySelector('.collapse');
                const bsCollapse = new bootstrap.Collapse(myCollapse, {
                    toggle: false
                });
                bsCollapse.hide();
            });
        } catch (e) {
            console.log('Multi-selector is in transition.')
            console.log(e);
        }
    }
}

function checkParentElementRecursive(elem, ...types) {
    if (elem === null) {
        return false;
    }

    if (elem.tagName === "HTML") {
        return false;
    }

    if (types.some(type => elem.getAttribute('elemType') === type)) {
        return true;
    } else {
        return checkParentElementRecursive(elem.parentElement, ...types);
    }
}

export function toggleTick(btn) {
    if (!btn) return;
    
    const svg = btn.querySelector('.tick-path');

    if (svg.classList.contains('drawn')) {
        svg.classList.remove('drawn');
    } else {
        svg.classList.add('drawn');
    }
}