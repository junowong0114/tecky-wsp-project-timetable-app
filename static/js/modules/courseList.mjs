import { Course } from './lib.mjs';

export async function getCourseList(courseTitleList){
    if(courseTitleList.length == 0){
        return [];
    }
    let queryString = courseTitleList.reduce((queryString, courseTitle) => {
        return queryString + (queryString === '?' ? '' : '&').concat(`subjectID=${courseTitle.subject_id}`).concat(`&courseCode=${courseTitle.course_code}`);
    }, '?');
    queryString = '/courses' + queryString;
    const res = await fetch(queryString);
    const result = await res.json();
    const courses = result.courses;
    const courseList = [];
    for(let course of courses){
        courseList.push(new Course(course))
    }
    return courseList;
}

export async function populateCourses(schoolFilters = [], subjectFilters = [], commonCoreFilter = false, areaFilters = []) {
    let queryString = schoolFilters.reduce((queryString, filter) => {
        return queryString + (queryString === '?' ? '' : '&').concat(`schoolFilter=${filter}`);
    }, '?');

    queryString = subjectFilters.reduce((queryString, filter) => {
        return queryString + (queryString === '?' ? '' : '&').concat(`subjectFilter=${filter}`);
    }, queryString);

    queryString = areaFilters.reduce((queryString, filter) => {
        return queryString + (queryString === '?' ? '' : '&').concat(`areaFilter=${filter === 's&t' ? 's%26t' : filter}`);
    }, queryString);

    queryString = queryString + (queryString === '?' ? '' : '&').concat(`commonCoreFilter=${commonCoreFilter}`);
    queryString = '/courses' + queryString;

    // console.log(queryString);
    document.querySelector('#spinner').classList.remove('hidden');
    deleteCourseList();

    const res = await fetch(queryString, {
        method: "GET"
    });
    const result = await res.json();

    if (result.success) {
        console.log(result);

        const courses = result.courses  // array of course objects (NOT instances of class Course)

        const courseObjs = [];  // array of class Course objects (as defined in class Course in ./lib.ts)
        /* convert courses to coursesObjs */
        for (let course of courses) {
            courseObjs.push(new Course(course));
        }
        renderCourseList(courseObjs);

        const res2 = await fetch('/starredCourses');
        const favCourses = (await res2.json()).courses;
        
        for (let course of favCourses) {
            const fullCourseCode = course.subject.toUpperCase() + ' ' + course.code.toUpperCase();

            const targetContainer = document.querySelector(`.position-relative[course="${fullCourseCode}"]`);
            if (targetContainer) {
                targetContainer.querySelector('path').classList.add('filled');
            }
        }       
    } else {
        document.querySelector('#spinner').classList.add('hidden');
    }
}

export function deleteCourseList() {
    const elements = document.querySelectorAll('#course-list-hero *');
    const numOfElem = elements.length;

    for (let i = 0; i < numOfElem; i++) {
        elements[i].remove();
    }
}

export function renderCourse(course, courseSessionIDsArray, sessionIDs) {
    let result = '';
    result = result.concat(
        `
        <div class="course-info">
            <div class="card card-body">
                <div class="row table-title-row">
                    <div class="col-lg-2">
                        <div class='table-item'>Activity</div>
                    </div>
                    <div class="col-lg-2">
                        <div class="table-item">Date(s)</div>
                    </div>
                    <div class="col-lg-2">
                        <div class="table-item">Time</div>
                    </div>
                    <div class="col-lg-3">
                        <div class="table-item">Room</div>
                    </div>
                    <div class="col-lg-3">
                        <div class="table-item">Instructor</div>
                    </div>
                    <div class="row-background"></div>
                </div>`
    ).concat(
        createActivityCardsWithRadio(course.getActivityData(), courseSessionIDsArray, sessionIDs, course.id)
    ).concat(
            `</div>
        </div>`
    )
    return result;
}

export function renderCourseList(courses) {
    const container = document.querySelector('#course-list-hero')

    let result = '';
    for (let course of courses) {
        result = result.concat(
            `<div class="position-relative" course="${course.subject.toUpperCase()} ${course.code.toUpperCase()}">
                <div class="course-toggle-btn collapsed no-select" type="button" data-bs-toggle="collapse"
                    data-bs-target="#${course.subject + course.code}" aria-expanded="false" aria-controls="${course.subject + course.code}">
                    <div class="row course-code-title">
                        <div class="col-lg-2">
                            <div class="course-code">${course.subject.toUpperCase()} ${course.code.toUpperCase()}</div>
                        </div>
                        <div class="col-lg-10">
                            <div class="course-title">${course.title}</div>
                        </div>
                    </div>
                    <div class="course-credit">${course.credit} credit(s)</div>
                </div>
                <div class="course-star">
                </div>
            </div>
            <div class="course-info collapse" id="${course.subject + course.code}">
                <div class="card card-body">
                    <div class="row table-title-row">
                        <div class="col-lg-2">
                            <div class='table-item'>Activity</div>
                        </div>
                        <div class="col-lg-2">
                            <div class="table-item">Date(s)</div>
                        </div>
                        <div class="col-lg-2">
                            <div class="table-item">Time</div>
                        </div>
                        <div class="col-lg-3">
                            <div class="table-item">Room</div>
                        </div>
                        <div class="col-lg-3">
                            <div class="table-item">Instructor</div>
                        </div>
                        <div class="row-background"></div>
                    </div>`
        ).concat(
            createActivityCards(course.getActivityData())
        ).concat(
                `</div>
            </div>`
        )
    }

    container.innerHTML = result;

    const starContainers = document.querySelectorAll('.course-star');
    
    for (let starContainer of starContainers) {
        const newStar = createStar();
        starContainer.appendChild(newStar);
    }
}

/* activityData is in the format of:
    [
        {fullName:string, dates: string, time: string, venue: string, instructor:string},
        {fullName:string, ...},
        ...
    ]
*/
function createActivityCards(activityData) {
    let result = '';

    for (let activity of activityData) {
        result = result.concat(createActivityCard(activity));
    }

    return result;
}

function createActivityCardsWithRadio(activityData, courseSessionIDsArray, sessionIDs, courseID) {
    let result = '';

    for (let index in activityData) {
        result = result.concat(createActivityCardWithRadio(activityData[index], courseSessionIDsArray[index], sessionIDs, courseID));
    }

    return result;
}

function createActivityCard(activity) {
    return `<div class="row course-card-row">
    <div class="col-lg-2">
        <div class="table-item">${activity.fullName}</div>
    </div>
    <div class="col-lg-2">
        <div class="table-item">${activity.dates}</div>
    </div>
    <div class="col-lg-2">
        <div class="table-item">${activity.time}</div>
    </div>
    <div class="col-lg-3">
        <div class="table-item">${activity.venue}</div>
    </div>
    <div class="col-lg-3">
        <div class="table-item">${activity.instructor}</div>
    </div>
    <div class="row-background"></div>
    </div>
    `;
}

function createActivityCardWithRadio(activity, courseSessionIDs, sessionIDs, courseID) {
    let activityFullName = activity.fullName;
    let activityType;
    switch (activityFullName.substring(0,2)) {
        case 'Le': 
            activityType = '0';
            break;
        case 'Tu':
            activityType = '1';
            break;
        case 'La':
            activityType = '2';
            break;
        case 'Re':
            activityType = '3';
            break;
    }
    let disableString = "";
    if(activity.time == 'TBA'){
        disableString = "disabled"
    }
    let checkedString = "";
    if(sessionIDs.length != 0){
        for(let sessionID of sessionIDs){
            if(courseSessionIDs[0] == sessionID){
                checkedString = "checked"
                break;
            }
        }
    }
    let name = courseID + "#" +activityType;

    return `<div class="row course-card-row">
    <div class="col-lg-2">
        <div class="table-item">${activity.fullName}</div>
    </div>
    <div class="col-lg-2">
        <div class="table-item">${activity.dates}</div>
    </div>
    <div class="col-lg-2">
        <div class="table-item">${activity.time}</div>
    </div>
    <div class="col-lg-3">
        <div class="table-item">${activity.venue}</div>
    </div>
    <div class="col-lg-2">
        <div class="table-item">${activity.instructor}</div>
    </div>
    <div class="form-check col-lg-1">
        <input class="form-check-input" type="radio" name="${name}" ${checkedString} ${disableString}>
    </div>
    <div class="row-background"></div>
    </div>
    `;
}

function createStar() {
    const newSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    newSVG.setAttribute('viewBox', '64 64 895 895');
    newSVG.setAttribute('focusable', 'false');
    newSVG.setAttribute('data0icon', 'start');
    newSVG.setAttribute('width', '1em');
    newSVG.setAttribute('height', '1em');
    newSVG.setAttribute('fill', '#898989');
    newSVG.setAttribute('aria-hidden', 'true');

    const newPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    newPath.setAttribute('d', 'M908.1 353.1l-253.9-36.9L540.7 86.1c-3.1-6.3-8.2-11.4-14.5-14.5-15.8-7.8-35-1.3-42.9 14.5L369.8 316.2l-253.9 36.9c-7 1-13.4 4.3-18.3 9.3a32.05 32.05 0 0 0 .6 45.3l183.7 179.1-43.4 252.9a31.95 31.95 0 0 0 46.4 33.7L512 754l227.1 119.4c6.2 3.3 13.4 4.4 20.3 3.2 17.4-3 29.1-19.5 26.1-36.9l-43.4-252.9 183.7-179.1c5-4.9 8.3-11.3 9.3-18.3 2.7-17.5-9.5-33.7-27-36.3z');
    newSVG.appendChild(newPath);

    return newSVG;
}