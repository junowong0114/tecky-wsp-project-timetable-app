export function createHTMLElement(tag, id = "", classes = "", attributes = "", text = "", children = "") {
    if (!typeof tag === 'string') return;
    const newElement = document.createElement(tag);

    if (id !== "") newElement.id = id;
    setElementClass(newElement, classes);
    setElementAttribute(newElement, attributes);
    newElement.innerHTML = text;
    appendChildren(newElement, children);

    return newElement;
}

export function appendChildren(element, children) {
    if (!assertHTMLElement(element)) return false;

    if (children instanceof Array) {
        for (let child of children) {
            appendChildren(element, child);
        }
    }

    if (children instanceof HTMLElement) {
        element.appendChild(children);
        return true;
    }
}

export function setElementClass(element, classes) {
    if (!assertHTMLElement(element)) return false;

    if (typeof classes === 'string' && classes !== '') {
        element.classList.add(classes);
    } else {
        element.classList.add(...classes);
    }

    return true;
}

export function setElementAttribute(element, attributes) {
    if (!assertHTMLElement(element)) return false;

    if (attributes instanceof Attribute) {
        element.setAttribute(attributes.name, attributes.value);
    }
    
    if (attributes instanceof Array) {
        for (let attribute of attributes) {
            setElementAttribute(element, attribute);
        }
    }
}

export function assertHTMLElement(testSubject) {
    return testSubject instanceof HTMLElement;
}

export class Attribute {
    constructor(name, value) {
        this.name = name,
        this.value = value
    }
}

export let courseCardType;
(function (courseCardType) {
    courseCardType[courseCardType["general"] = 0] = "general";
    courseCardType[courseCardType["arts"] = 1] = "arts";
    courseCardType[courseCardType["c-comm"] = 2] = "c-comm";
    courseCardType[courseCardType["e-comm"] = 3] = "e-comm";
    courseCardType[courseCardType["hlth"] = 4] = "hlth";
    courseCardType[courseCardType["h"] = 5] = "h";
    courseCardType[courseCardType["qr"] = 6] = "qr";
    courseCardType[courseCardType["sat"] = 7] = "sat";
    courseCardType[courseCardType["sa"] = 8] = "sa";
})(courseCardType || (courseCardType = {}));