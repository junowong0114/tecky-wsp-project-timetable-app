let alert = document.querySelector('#alert-container');

window.onload = () => {
    loadStudentName();
}

async function loadStudentName() {
    let res = await fetch('/name');
    let result = await res.json();
    console.log(result)

    let studInfo = Object.values(result)

    document.querySelector("#first_name").placeholder = studInfo[1];
    document.querySelector("#last_name").placeholder = studInfo[2];
    document.querySelector("#first_name").value = studInfo[1];
    document.querySelector("#last_name").value = studInfo[2];
}

document.querySelector('#update-form').onsubmit = async function (event) {
    const form = event.target;
    event.preventDefault();

    if(form.confirm_new_password.value != form.new_password.value){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">New password and confirmed new password are not the same</div>'
        return;
    }
    if(form.new_password.value == "" | form.confirm_new_password.value == ""){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">Password cannot be null</div>'
        return;
    }
    const formObj = {
        first_name: form.first_name.value,
        last_name: form.last_name.value,
        old_password: form.old_password.value,
        password: form.new_password.value,
    }
    console.log(formObj)
    let res = await fetch('/updateStudentInfo', {
        method: "PUT",
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(formObj)
    });
    const result = await res.json();
    if(result.success){
        alert.innerHTML = '<div class="alert alert-danger" role="alert">You have successfully edit profile and password</div>'
    }
    else{
        alert.innerHTML = '<div class="alert alert-danger" role="alert">Wrong old password</div>'

    }
    
   
}