let alert = document.querySelector('#alert-container');

document.querySelector('#signup-form').onsubmit = async function (event) {
    const form = event.target;
    event.preventDefault();
    
    const formObj = {
        first_name: form.first_name.value,
        last_name: form.last_name.value,
        username: form.username.value,
        password: form.password.value,
        confirm_password: form.confirm_password.value
    }
    alert.innerHTML ='';
    let errorBool = 0;
    if(form.first_name.value == ""){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">First name cannot be null</div>'
        errorBool = 1;
    }
    if(form.last_name.value == ""){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">Last name cannot be null</div>'
        errorBool = 1;
    }
    if(form.username.value == ""){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">Username cannot be null</div>'
        errorBool = 1;
    }

    if(form.password.value == "" | form.confirm_password.value == ""){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">Password cannot be null</div>'
        errorBool = 1;
    }
    else if(form.password.value != form.confirm_password.value){
        alert.innerHTML += '<div class="alert alert-danger" role="alert">Password does not match</div>'
        errorBool = 1;
    }

    if(errorBool == 1){
        return;
    }

    const res = await fetch('/signup', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObj),
        credentials: 'include'
    });
    const result = await res.json();
    console.log(result);

    if (result.success) {
        // login. 轉去/timetable
        // 話比user聽成功sign up (alert, bootstrap modal form, bootstrap alert)
        window.location = '/timetable';
    } else {
        // 失敗
        // 話比user聽衰咩(留喺同一頁) (result.msg)
        alert.innerHTML = `
            <div class="alert alert-danger" role="alert">
                ${result.msg}  
            </div>
         `;
    }
}