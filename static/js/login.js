document.querySelector('#login-form').onsubmit = async function (event) {
    const form = event.target;

    event.preventDefault();

    const formObj = {
        username: form.username.value,
        password: form.password.value
    }

    const res = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObj),
        credentials: 'include'
    });

    const result = await res.json();
    console.log(result);

    if (result.success) {
        window.location = '/timetable';
    } else {
        document.querySelector('#alert-container').innerHTML = `
        <div class="alert alert-danger" role="alert">
            ${result.msg}  
        </div>
        `;
    }
}