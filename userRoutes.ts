import express from 'express';
import { client } from './app';
import { checkPassword } from './hash';
import { User } from './models';
import { isLoggedIn } from './guard';
import { hashPassword } from './hash';

export const userRoutes = express.Router();

userRoutes.get('/', (req, res) => {
    const page = 'index';
    res.render('public/index', { page: page });
})

userRoutes.get('/login', (req, res) => {
    const page = 'login';
    res.render('public/login', { page: page });
})

userRoutes.post('/login', async (req, res) => {
    const { username, password } = req.body;

    let users: User[] = (await client.query('SELECT * from students where username = $1',
        [username])).rows;
    const user = users[0];

    if (user && await checkPassword(password, user.password)) {
        req.session['user'] = user;
        req.session['student_id'] = user.student_id;
        res.status(200).json({ success: true });
    } else {
        res.status(401).json({ success: false, msg: 'Incorrect username/password.' });
    }
})

userRoutes.get('/signup', (req, res) => {
    const page = 'signup';
    res.render('public/signup', { page: page });
})

userRoutes.post('/signup', async (req, res) => {
    console.log(req.body);
    const { first_name, last_name, username, password, confirm_password } = req.body;

    console.log(`${first_name} ${last_name} ${username} ${password} ${confirm_password}`);
    // 如果password !== confirm_password
    // res.status(400).json( {success: false, msg: 'Passwords does not match' });
    if (password !== confirm_password) {
        res.status(400).json({ success: false, msg: 'Passwords does not match' });
    } else {
        // check database 本身有無username存在
        // 有的話
        //res.status(400).json({ success: false, msg: 'Username already exist' });
        const numOfMatchedUser = (await client.query('SELECT username FROM students WHERE username = $1', [username])).rowCount;
        if (numOfMatchedUser !== 0) {
            res.status(400).json({ success: false, msg: 'Username already exist' });
        }
        else {
            // hash password
            let passwordHash = await hashPassword(password);
            console.log(passwordHash)

            // 將username first_name last_name password(hash左) 射上server
            // res.json( {success: true });
            await client.query('INSERT into students(first_name, last_name, username, password) VALUES ($1, $2 , $3, $4)', [first_name, last_name, username, passwordHash])

            res.json({ success: true });
        }
    }
});

userRoutes.get('/configure', isLoggedIn, (req, res) => {
    const page = 'configure';
    res.render('protected/configure', { page: page, user: req.session['user'] });
})

userRoutes.get('/registered', (req, res) => {
    const page = 'registered';
    res.render('protected/registered', { page: page, user: req.session['user'] });
})

userRoutes.get('/logout', async (req, res) => {
    req.session.destroy((err)=>{});
    res.redirect('/');
});

userRoutes.get('/editprofile', isLoggedIn, (req, res) => {
    const page = 'editprofile';
    res.render('protected/editprofile', { page: page, user: req.session['user'] });
})


userRoutes.get('/name', async (req, res) => {
    let result = await client.query('SELECT student_id, first_name, last_name, password FROM students where student_id = $1', [req.session['student_id']]);
    let name = result.rows[0];
    res.json(name);
    console.log(name);

});


userRoutes.put('/updateStudentInfo', async (req, res) => {
    let formObj = await req.body;
    console.log(formObj)

    let result = await client.query('SELECT password FROM students WHERE student_id= $1', [req.session['student_id']]);
    let resultPassword = await result.rows[0];
    let bool = await checkPassword(formObj.old_password, resultPassword.password);
    if(!bool){
        res.status(400).json({success:false});
        return;
    }
    let passwordHash = await hashPassword(formObj.password);
    await client.query('UPDATE students SET first_name = $1, last_name = $2, password = $3  where student_id = $4', [formObj.first_name, formObj.last_name, passwordHash, req.session['student_id']]);
    res.status(200).json({success:true});
});


