import playwright from 'playwright';
import jsonfile from 'jsonfile';
import { CommonCore } from './model';

async function main() {
    const browser = await playwright.chromium.launch({ headless: false });
    const context = await browser.newContext();

    /* start new page */
    const page = await context.newPage();

    /* go to ust course page */
    await page.goto('https://w5.ab.ust.hk/wcq/cgi-bin/2030/common_core/4Y');
    await page.click('button.close');

    /* Scrape common cores */
    const areas = { 
                    "Arts": "Arts",
                    "C-Comm": "Chinese Communication",
                    "E-Comm": "English Communication",
                    "HLTH": "Healthy Lifestyle",
                    "H": "Humanities",
                    "QR": "Quantitative Reasoning",
                    "S&amp;T": "Science and Technology",
                    "SA": "Social Analysis",
                    "other": "Other"
                }
    const commonCores: CommonCore[] = [];
        
    for (let pageIndex = 12; pageIndex < 20; pageIndex++) {
        await page.goto(`https://w5.ab.ust.hk/wcq/cgi-bin/2030/common_core/4Y/${pageIndex}`)
        await page.waitForLoadState('networkidle');

        /* get common core area */
        const areaKey = await page.evaluate(([pageIndex]) => {
            const pageLink = document.querySelector(`li.crseattrsearchlink ul.submenu li:nth-child(${pageIndex - 8}) a`)
            if (!pageLink) return "other";

            const areaKeyRegEXResultArray = pageLink.innerHTML.match(/\((.+)\)/);
            if (!areaKeyRegEXResultArray) return "other";

            return areaKeyRegEXResultArray[1];
        }, [pageIndex]);
        const area = areas[areaKey];

        /* scrape common core courses from page */
        const courseStrings = await page.$$eval('.course', (table) => {
            const numOfCourses = table.length;
    
            const courseResults = [];
    
            for (let i = 0; i < numOfCourses; i++) {
                const course = table[i];
    
                const title = course.querySelector('h2')?.innerText;
                if (title) courseResults.push(title);
            }
            return courseResults;
        });
        const numOfCourses = courseStrings.length;

        /* post-process $$eval result */
        for (let j = 0; j < numOfCourses; j++) {
            const commonCore: CommonCore = {
                subject: courseStrings[j].slice(0, 4),
                code: (courseStrings[j].split(' '))[1],
                area: area
            }
            commonCores.push(commonCore);
        }
    }

    await jsonfile.writeFile('./common_core_courses.JSON', commonCores, { spaces: 4 });
    await browser.close();
}

main();