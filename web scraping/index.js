//require('ts-node/register');
//require('./app');

// const playwright = require('playwright');
// async function main() {
//     const browser = await playwright.chromium.launch({
//         headless: false // setting this to true will not run the UI
//     });
    
//     const page = await browser.newPage();
//     await page.goto('https://finance.yahoo.com/world-indices');
//     await page.waitForTimeout(5000); // wait for 5 seconds
//     await browser.close();
// }

// main();



   
   
   
   
// const playwright = require('playwright');
//     async function mostActive() {
//         const browser = await playwright.chromium.launch({
//             headless: true // set this to true
//         });
        
//         const page = await browser.newPage();
//         await page.goto('https://w5.ab.ust.hk/wcq/cgi-bin/2040/subject/ACCT');
//         const mostActive = await page.$eval('#classes tbody', tableBody => {
//             let all = []
//             for (let i = 0, row; row = tableBody.rows[i]; i++) {
//                 let stock = [];
//                 for (let j = 0, col; col = row.cells[j]; j++) {
//                     stock.push(row.cells[j].innerText)
//                 }
//                 all.push(stock)
//             }
//             return all;
//         });
        
//         console.log('Most Active', mostActive);
//         //await page.waitForTimeout(30000); // wait
//         await browser.close();
//     }
    
//     mostActive();





const playwright = require('playwright');

async function main() {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: false
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    // Tell the tab to navigate to the GitHub Topics page.
    await page.goto('https://w5.ab.ust.hk/wcq/cgi-bin/2040/subject/ACCT');
    // Click and tell Playwright to keep watching for more than
    // 30 repository cards to appear in the page.
    await page.click('text=Load more');
    await page.waitForFunction(() => {
        const repoCards = document.querySelectorAll('article.border');
        return repoCards.length > 30;
    });
    // Pause for 10 seconds, to see what's going on.
    await page.waitForTimeout(10000);
    // Turn off the browser to clean up after ourselves.
    await browser.close();
}

main();