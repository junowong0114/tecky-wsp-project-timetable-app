export function setTimeoutPromise(timeOut: number) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, timeOut);
    });
}