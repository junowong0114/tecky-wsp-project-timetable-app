export interface Course {
    subject?: string;
    code?: string;
    title?: string;
    credit?: number;
}

export interface CommonCore {
    subject?: string;
    code?: string;
    area: string;
}

export interface Subject {
    name: string;
}

export enum ActivityType {
    lecture = 0,
    tutorial,
    lab,
    other
}

export interface Activity {
    course_subject?: string
    course_code?: string
    name?: string
    type?: ActivityType
    instructor?: string
    venue?: string
    weekday?: number
    start_time?: string
    end_time?: string
}