import playwright from 'playwright';
import jsonfile from 'jsonfile';
import { Subject, Course, Activity, ActivityType } from './model';

async function main() {
    const browser = await playwright.chromium.launch({ headless: false });
    const context = await browser.newContext();

    /* start new page */
    const page = await context.newPage();

    /* go to ust course page */
    await page.goto('https://w5.ab.ust.hk/wcq/cgi-bin/2030/');
    await page.click('button.close');
    await page.hover('li.term');
    await page.click('a:text("2020-21 Spring")');
    await page.waitForLoadState('networkidle');

    /* scrape subjects */
    const subjects = await scrapeSubjects(page);
    jsonfile.writeFile('./subjects.JSON', subjects, { spaces: 4 });

    /* scrape courses and activities */
    let courses: Course[] = [];
    let activities: Activity[] = [];
    
    const numOfSubjects = subjects.length;
    for (let n = 0; n < numOfSubjects; n++) {
        const subject = subjects[n];
        await page.click(`.depts a:text("${subject.name}")`);
        await page.waitForLoadState('networkidle');
        
        const { courseStrings, activityStrings } = await page.$$eval('.course', (table) => {
            const numOfCourses = table.length;
    
            const activityResults = [];
            const courseResults = [];
    
            for (let i = 0; i < numOfCourses; i++) {
                const course = table[i];
    
                const title = course.querySelector('h2')?.innerText;
                if (title) courseResults.push(title);
    
                const activities = course.querySelectorAll('.sections tr');
                const numOfSections = activities.length;
    
                let lastSectionName = '';
    
                for (let j = 1; j < numOfSections; j++) {
                    const activity = activities[j];
    
                    const activityData = activity.querySelectorAll('td');
                    const numOfData = activityData.length;
    
                    let name = '';
                    let dateAndTime = '';
                    let venue = '';
                    let instructor = '';
    
                    if (numOfData === 3) {
                        name = lastSectionName;
                        dateAndTime = activityData[0].innerText;
                        venue = activityData[1].innerText;
                        instructor = activityData[2].innerText;
                    } else {
                        name = activityData[0].innerText;
                        dateAndTime = activityData[1].innerText;
                        venue = activityData[2].innerText;
                        instructor = activityData[3].innerText;
                        lastSectionName = name;
                    }
    
                    activityResults.push([title, name, dateAndTime, venue, instructor]);
                }
            }
            return { courseStrings: courseResults, activityStrings: activityResults };
        })

        /* post process scraped courses strings */
        courses = courses.concat(await jsonifyCourseStrings(courseStrings));
        
        /* post process scrpaed activities strings */
        activities = activities.concat(await jsonifyActivityStrings(activityStrings));
    }
     
    await jsonfile.writeFile('./courses.JSON', courses, { spaces: 4 });
    await jsonfile.writeFile('./activities.JSON', activities, { spaces: 4 });

    await browser.close();
}

async function scrapeSubjects(page: playwright.Page) {
    const subjects = await page.evaluate(() => {
        const subjects: Subject[] = [];

        const subjectLinks = document.querySelectorAll('.depts a.ug');
        const numOfSubjects = subjectLinks.length;

        for (let i = 0; i < numOfSubjects; i++) {
            subjects.push({
                name: subjectLinks[i].innerHTML
            });
        }

        return subjects;
    });
    return subjects;
}

async function jsonifyCourseStrings(courseStrings: string[]) {
    const courses: Course[] = [];

    for (let courseString of courseStrings) {
        /* search for '3 units)' pattern in courseString by regex */
        let creditString = courseString.match(/(\d+) units*\)$/);
        if (!creditString) {
            console.log(`Credit information not found in ${courseStrings}!`);
            continue;
        }

        /* search for '-' in courseString pattern by regex */
        let dashLocation = courseString.indexOf('-');
        if (dashLocation === -1) {
            console.log(`Dash not found in course ${courseStrings}!`);
            continue;
        }

        const course: Course = {
            subject: courseString.slice(0, 4),
            code: courseString.slice(5, dashLocation - 1),
            title: creditString && creditString['index'] ? courseString.slice(dashLocation + 2, creditString['index'] - 2) : undefined,
            credit: creditString ? parseInt(creditString['1']) : undefined
        };

        courses.push(course);
    }

    return courses;
}

async function jsonifyActivityStrings(activityStrings: (string | undefined)[][]) {
    const activities: Activity[] = [];

    for (let activityString of activityStrings) {
        if (!activityString[0] || !activityString[1] || !activityString[2] || !activityString[3] || !activityString[4]) {
            continue;
        }

        /* example: 'ACCT 2200 - Principles of Accounting II (3 units)' */
        const dashLocation = activityString[0].indexOf('-');
        if (!dashLocation) continue;

        /* example: 'L04 (1029)' */
        let type: ActivityType;
        if (activityString[1].match(/^L[^A]/)) {
            type = ActivityType.lecture;
        } else if (activityString[1].match(/^LA+/)) {
            type = ActivityType.lab;
        } else if (activityString[1].match(/^T/)) {
            type = ActivityType.tutorial;
        } else {
            type = ActivityType.other;
        }

        /* example: 'WeFr 01:30PM - 02:50PM' */
        const weekdays: number[] = [];
        if (activityString[2].match(/Mo/)) {    // Monday
            weekdays.push(0);
        }
        if (activityString[2].match(/Tu/)) {    // Tuesday
            weekdays.push(1);
        }
        if (activityString[2].match(/We/)) {    // Wednesday
            weekdays.push(2);
        }
        if (activityString[2].match(/Th/)) {    // Thursday
            weekdays.push(3);
        }
        if (activityString[2].match(/Fr/)) {    // Friday
            weekdays.push(4);
        }
        if (weekdays.length === 0) {            // TBA
            weekdays.push(5);
        }

        /* possible cases
            '03-APR-2021 - 10-APR-2021\nSa 09:00AM - 12:20PM'
            'Mo 03:00PM - 04:20PM'
            'TuTh 01:30PM - 02:50PM'
            'TBA' */
        const durationString = ((activityString[2].split('\n')).splice(-1))[0]  // '01:30PM - 02:50PM'
        let startTime: string;
        let endTime: string;

        if (durationString === 'TBA') {
            startTime = 'TBA';
            endTime = 'TBA'
        } else {
            startTime = convertTime12to24(durationString.split(' ')[1]);                   // '01:30PM'
            endTime = convertTime12to24(durationString.split(' ').splice(-1)[0]);          // '02:50PM'
        }

        for (let weekday of weekdays) {
            const activity: Activity = {
                course_subject: activityString[0].slice(0, 4), /* example: 'ACCT 2200 - Principles of Accounting II (3 units)' */
                course_code: activityString[0].slice(5, dashLocation - 1), /* example: 'ACCT 2200 - Principles of Accounting II (3 units)' */
                name: activityString[1].split(' ')[0].replace(' ', ''),
                type: type,
                instructor: (activityString[4].split('\n'))[0], /* example: 'CHEN, Yanzhen\nHUANG, Allen Hao' */
                venue: activityString[3],
                weekday: weekday,
                start_time: startTime,
                end_time: endTime
            }

            activities.push(activity);
        }
    }

    return activities;
}

/* input format HH:MMAM / HH:MMPM */
function convertTime12to24(time12h: string) {
    let time = time12h.slice(0, time12h.length - 2);
    time = time.replace(' ', '');

    const modifierRegEXMatchArray = time12h.match(/[PA]+M$/);
    if (!modifierRegEXMatchArray) return 'TBA';
    const modifier = modifierRegEXMatchArray[0];

    let [hours, minutes] = time.split(":");

    if (hours === "12") {
        hours = "00";
    }

    if (modifier === "PM") {
        hours = parseInt(hours, 10) + 12 + "";
    }

    return `${hours}:${minutes}`;
}

main();