import bcrypt from 'bcryptjs';

const SALT_ROUND = 10;

export async function hashPassword(plainPassword: string) {
    return await bcrypt.hash(plainPassword, SALT_ROUND);
}

export async function checkPassword(plainPassword: string, hashPassword: string) {
    return await bcrypt.compare(plainPassword, hashPassword);
}
